/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    $(".card").click(function(){
        if($(this).attr("data-seg") == "moda"){
            
            
            $("#conteudoModalSegmentos").html("<h3><b>Moda</b></h3>Softwares especializado para lojas, \n\
              redes ou franquias de Roupas, Calçados, Bolsas e Acessórios que precisam organizar e controlar \n\
              processos de caixa, gestão de produtos em grades, gestão administrativa, gestão financeira e fiscal.\n\
              A Mais Sistemas Inteligentes compreende os seus desafios e está comprometida em lhe ajudar a alcança-los. \n\
              Disponibilizamos recursos que facilitam interpretar tendências de consumo, através da gestão eficiente de seu negócio promovendo o \n\
              crescimento a maximização de sua lucratividade.");
            $("#myModal").modal("show");
        }
        else if($(this).attr("data-seg") == "supermercado"){
                        
            $("#conteudoModalSegmentos").html("<h3><b>Supermecado</b></h3>Softwares voltado para lojas, redes ou franquias de Supermercados, \n\
                Minimercados e Conveniência que precisam organizar e controlar processos de caixa, gestão de produtos em grades, \n\
                gestão administrativa, gestão financeira e fiscal. A Mais Sistemas Inteligentes compreende os seus desafios e está \n\
                comprometida em lhe ajudar a alcança-los. Disponibilizamos recursos que facilitam interpretar tendências de consumo, \n\
                através da gestão eficiente de seu negócio promovendo o crescimento a maximização de sua lucratividade.");
            $("#myModal").modal("show");
        }
        else if($(this).attr("data-seg") == "alimenticio"){
                        
            $("#conteudoModalSegmentos").html("<h3><b>Alimentício</b></h3>Softwares voltado para lojas, \n\
                redes ou franquias de Lanchonetes e Restaurantes que precisam organizar \n\
                e controlar processos de caixa, gestão de produtos em grades, gestão administrativa, \n\
                gestão financeira e fiscal. A Mais Sistemas Inteligentes compreende os seus desafios e está comprometida \n\
                em lhe ajudar a alcança-los. Disponibilizamos recursos que facilitam interpretar tendências de consumo, \n\
                através da gestão eficiente de seu negócio promovendo o crescimento a maximização de sua lucratividade.");
            $("#myModal").modal("show");
        }
        else if($(this).attr("data-seg") == "lar_decoracao"){
                        
            $("#conteudoModalSegmentos").html("<h3><b>Lar & Decoração</b></h3>Softwares voltado para lojas, redes ou franquias de Cama, Mesa, Banho e Decoração do Lar que precisam organizar e controlar processos de caixa, gestão de produtos em grades, gestão administrativa, gestão financeira e fiscal.");
            $("#myModal").modal("show");
        }
//        else if($(this).attr("data-seg") == "auto_pecas"){
//                        
//            $("#conteudoModalSegmentos").html("");
//            $("#myModal").modal("show");
//        }
        else if($(this).attr("data-seg") == "cosmeticos"){
                        
            $("#conteudoModalSegmentos").html("<h3><b>Cosméticos</b></h3> Softwares voltado para lojas, redes ou franquias de Cosméticos, Perfumaria e Higiene Pessoal que precisam organizar e controlar processos de caixa, gestão de produtos em grades, gestão administrativa, gestão financeira e fiscal. A Mais Sistemas Inteligentes compreende os seus desafios e está comprometida em lhe ajudar a alcança-los. Disponibilizamos recursos que facilitam interpretar tendências de consumo, através da gestão eficiente de seu negócio promovendo o crescimento a maximização de sua lucratividade.");
            $("#myModal").modal("show");
        }
    });
});
