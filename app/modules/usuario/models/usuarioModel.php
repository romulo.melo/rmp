<?php

class usuarioModel extends AppModel{
    
    public function listar_usuarios() {
        
        return $this->read("*, date_format(dtUltimoAcesso,'%d/%m/%Y') as dtUltimoAcesso, time(dtUltimoAcesso) as horaUltimoAcesso", "tbusuarios");
    }
    
    public function cadastrar_usuario($post){
        
        $novoUsuario['nmUsuario'] = $post['nmUsuario'];
        $novoUsuario['login'] = $post['email'];
        $novoUsuario['senha'] = md5($post['senha']).SALT;
        $novoUsuario['token'] = md5(date("Y-m-d H:i:s"). rand(1, 99));                
        
        if($this->insert("tbusuarios", $novoUsuario)){
            
            return true;
        }
        else{
            return false;
        }
       
    }
    
    public function achar_email($email){
        //o login é o email
        if($this->read("login", "tbusuarios", "login = '{$email}'")){
            return true;
        }
        else{
            return false;
        }
    }
}

