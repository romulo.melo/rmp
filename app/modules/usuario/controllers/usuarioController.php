<?php

class usuario extends AppController{
    
    private  $usuario;

    public function __construct() {
        
        $this->usuario = new usuarioModel();
        
        parent::__construct();
    }
    
    public function index_action() {
                        
    }
    
    public function listar() {
        
        $listarUsuarios = $this->usuario->listar_usuarios();
        
        $this->authenticated_view("listar_usuarios", $listarUsuarios);
    }
    
    public function cadastrar(){
        
        if(!empty($_POST)){
            
           $dados = $this->anti_injection($_POST);
           
           if( (!empty($dados['nmUsuario'])) && (!empty($dados['email'])) && (!empty($dados['senha'])) && (!empty($dados['confirmeSenha'])) ){
               
               
               //verifica se o email informado para login já existe
               if($this->usuario->achar_email($dados['email'])){
                   
                   echo "Email informado já existe";
                   exit();
               }
               
               if($dados['senha'] == $dados['confirmeSenha']){
                                                         
                   if($this->usuario->cadastrar_usuario($dados)){
                       
                       $this->authenticated_view("cadastrar_usuario", "cadastro_true");
                   }
                   else{
                       $this->authenticated_view("cadastrar_usuario", "cadastro_false");
                   }
               }
           }
           echo 'preencha todos os campos';
           
        }
        else{
        
            $this->authenticated_view("cadastrar_usuario");
        }
        
    }
    
    public function verificar_email() {
        
        $email = $this->anti_injection($_GET['email']);
        
        if(!$this->usuario->achar_email($email))
        {
            echo 1;
        }
        else{
            echo 0;
        }
        
        
    }
    
}