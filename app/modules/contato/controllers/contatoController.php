<?php

class contato extends AppController {

    private $contato;

    public function __construct() {

        $this->contato = new contatoModel();

        parent::__construct();
    }

    public function index_action() {

        $this->view("contato");
    }

    public function enviar_email() {

        if (!empty($_POST)) {

            $contato = $this->anti_injection($_POST);

            if ($this->contato->enviar_email($contato)) {

                $this->view("contato", "email_true");
            } else {

                $this->view("contato", "email_false");
            }
        }
    }

}
