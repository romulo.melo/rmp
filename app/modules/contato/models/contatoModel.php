<?php

class contatoModel extends AppModel {

    public function enviar_email($dados) {

        $destinatario = "comercial@sistemasmais.net";

        $assunto = "Mensagem enviada pelo formulário do Site Mais Sistemas";

        $conteudo = "<b>Nome:</b> {$dados['nome']} <br><br>"
                . "<b>Email:</b> {$dados['email']} <br><br>"
                . "<b>Empresa: </b> {$dados['empresa']}<br><br>"
                . "<b>Telefone: </b> {$dados['telefone']} <br><br>"
                . "<b>Mensagem: </b>{$dados['msg']}";

        return $this->sendMail($destinatario, $assunto, $conteudo);
    }

}
