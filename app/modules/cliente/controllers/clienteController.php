<?php

class cliente extends AppController {
    
    private $cliente;

    public function __construct() {
                
        $this->cliente = new clienteModel();
        
        parent::__construct(); 
    }
    
    /*
     * NESSA ACTION EU FAÇO A IDENTIFICAÇÃO DE QUAL AÇÃO SERÁ REALIZADA NO MEU SERVIÇO DE CLIENTE
     * 
     * A IDENTIFICAÇÃO É FEITA ATRAVÉS DE UM MODO ENVIADO NO ARQUIVO JSON
    */
    public function index_action(){  
        
        //DEFINO NO CABEÇALHO A APLICAÇÃO COMO JSON
        //header('Content-type: application/json');
        
        //PEGO A ARQUIVO ENVIADO NO CORPO DA REQUISIÇÃO
        $request = file_get_contents("php://input") != '' ? file_get_contents("php://input"): NULL;
                           
        //TRANSFORMO O JSON ENVIADO NUM VETOR
        $json = (array) json_decode( $request );  
        
        //SE O JSON FOR INVÁLIDO OU A REQUISIÇÃO, PARA A EXECUÇÃO. 
        if(($this->validar_json() != TRUE) || ($request == NULL) ){
            
            echo 'ERROR JSON INVALIDO';
            exit();
        }
        
        //VERIFICO O MODO QUE FOI ENVIADO, SE NÃO TIVER RETORNA ERRO        
        switch (@$json['modo']) {
            
            case 'solicita_licenca':
                //SOLICITAR A LICENÇA
                echo @$this->solicitar_licenca($json);
                
                break;
            
            case 'confere_simples':

                echo @$this->confere_simples($json['idCliente']);

                break;  
            
            case 'listaclientes':

                echo @$this->confere_lista($json['autorizar']);

                break; 
            
            case 'cadastro':
                
                echo @$this->cadastro($json);
                
                break;
            
            case 'bloqueia':
                
                echo @$this->bloqueia($json);
                
                break;
            
            case 'autoriza':
                
                $this->autoriza($json);
                
                break;
            
            default:
                
                echo json_encode(array('Error'=>'Invalid Request'));
                
                break;
        }                
    }
//***************************************** -- FIM DA FUNÇÃO INDEX -- ***************************************//
   
    public function autoriza($post = NULL) {
        //SE VIER DA API
        if((!is_null($post)) && ($post != '') ){
            
            $post = $this->anti_injection($post);
            
            $setAutoriza = $this->cliente->autoriza($post);
            
            $jsonSetAutoriza = json_encode($setAutoriza, JSON_UNESCAPED_UNICODE);
            
            $this->view("cliente", $jsonSetAutoriza);
        } 
        else{
            //SE VIER DO FORMULÁRIO
            if($this->cliente->autenticar_usuario()){
                                
                if(!empty($_POST)){
                    
                $dados = $_POST;    
                  
                //CASO SEJA NECESSÁRIO UTILIZAR O PERFIL DE USO QUE ESTÁ COMENTADO NO RENOVAÇÃO DA LICENÇA
                //$dados['tpPerfil']  = isset($_POST['frenteLoja'])? $_POST['frenteLoja']:"0";
                //$dados['tpPerfil'] .= isset($_POST['fiscal'])? $_POST['fiscal']:"0";
                //$dados['tpPerfil'] .= isset($_POST['retaguarda'])? $_POST['retaguarda']:"0";
                
                //status da licenca 1 = ativo
                $dados['tpStatus'] = 1;
                
                $dados['dtAtualizacao'] = strtotime($_POST['dtAtualizacao']);
                
                if(!isset($_SESSION)){
                    
                    session_start();
                }
                
                $dados['nmUsuario'] = $_SESSION['nmUsuario'];
                
                $renovar = $this->cliente->autoriza($dados);
                
                //VOLTA PARA A PÁGINA DE RENOVAR LICENÇA
                $getCliente = $this->cliente->buscar_cliente_renovar($dados['idCliente']);
                  
                header("location:".URL_BASE."cliente/autoriza/idCliente/".$dados['idCliente']."?autoriza=true");
                                
                }
                else{
                    @$idClienteVet = $this->getParams();
                    
                    if(isset($idClienteVet['idCliente'])){
                        
                        $idCliente = $idClienteVet['idCliente'];
                        $getCliente = $this->cliente->buscar_cliente_renovar($idCliente);
                        $this->authenticated_view("renovar_licenca",$getCliente);
                    }
                }
                
            }else{
                echo "acesso negado!";
            }           
        }
    }
       
    public function bloqueia($post = NULL) {
        //SE VIER DA API
        if ((!is_null($post)) && ($post != '')) {

            $post['idCliente'] = $this->anti_injection($post['idCliente']);            

            $post['dtBloqueio'] = $this->anti_injection($post['dtBloqueio']);
                      
            $bloquear = $this->cliente->bloqueia($post);

            return $this->view("cliente", stripslashes(json_encode($bloquear, JSON_UNESCAPED_UNICODE)));
        }
        //SE VIER DO FORMULÁRIO
        else{
            
           
            if((!empty($_POST) && ($_POST != NULL)) ){                 

                $idCliente = $this->anti_injection($_POST['idCliente']);
                
                if(!isset($_SESSION)){
                
                    session_start();
                }

                $bloquear['nmUsuario'] = $_SESSION['nmUsuario'];
                
                $bloquear['idCliente']  = $idCliente;
                $bloquear['observacao'] = $_POST['motivoBloqueio'];
                $bloquear['dtBloqueio'] = date("Y-m-d H:i:s");
                
                
                $setBloquear = $this->cliente->bloqueia($bloquear);
                
                $this->authenticated_view("cliente",$setBloquear['status']); 
            }
        }
    }
    
    public function cadastro($post = NULL) {
        
        //se vier pela API
        if ((!is_null($post)) && ($post != '')) {

            unset($post['modo']);

            $post = $this->anti_injection($post);

            return json_encode($this->cliente->cadastrar($post));
        }
        else //se acessar a função diretamente, vem do formulário.
        {
            if(!empty($_POST)){
                
                $post = $this->anti_injection($_POST);
                
                //DADOS DO CLIENTE CLIENTE  TBCLIENTE
                $dadosCliente['cnpj'] = $post['cnpj'];
                $dadosCliente['nmFantasia'] = $post['nmFantasia'];
                $dadosCliente['apelido'] = $post['apelido'];
                $dadosCliente['qtdDispositivos'] = $post['qtdDispositivos'];
                $dadosCliente['dtValidade'] = $post['dtValidade'];
                
                //MÓDULOS DO CLIENTE - TBMODULOCLIENTE
                //deixo somente os módulos
                unset($post['cnpj']);
                unset($post['nmFantasia']);
                unset($post['apelido']);
                unset($post['qtdDispositivos']);
                unset($post['dtValidade']);                
                $modulos = $post;                  
                
                //VERIFICO SE O CNPJ JÁ EXISTE
                if($this->cliente->consulta_cnpj($dadosCliente['cnpj'])){
                    
                    //header("location:".URL_BASE."cliente/cadastro?cnpj=true");
                    //echo "<script>alert('O CNPJ: ".$dadosCliente['cnpj']." já está cadastrado.'); history.back();</script>";
                    $this->authenticated_view("cadastrar_cliente", $this->cliente->listar_modulos(),$_POST,"cnpj_existe");
                    exit();
                }                                                           
                
                //VERIFICO SE EXISTE SESSAO
                if(!isset($_SESSION)){
                    
                    session_start();
                }
                $dadosCliente['nmInsercao'] = $_SESSION['nmUsuario'];
                $idUsuario = $_SESSION['idUsuario'];                
                
                $cadastro = $this->cliente->cadastrar($dadosCliente, $modulos, $idUsuario); 
                                                                                           
                $cadastrar = $cadastro['status'];               
                
                header("location:".URL_BASE."cliente/cadastro?status={$cadastrar}");
                
                //$this->authenticated_view("cadastrar_cliente",$cadastrar);
            }
            else{
                
                $modulos = $this->cliente->listar_modulos();
                
                $this->authenticated_view("cadastrar_cliente",$modulos);
            }
        }
    }
        
    public function solicitar_licenca($post = NULL) {                              
        
        //VERIFICO SE O IDCLIENTE E O IDDISPOSITIVO ESTÁ VAZIO OU NULO
        if( (is_null($post['idCliente'])) || ($post['idCliente'] == '') || (is_null($post['idDispositivo'])) || ($post['idDispositivo'] == '') ){
            
            $jsonRetorno = array("status"=>-2, "msg"=>"dados incompletos");
            
            return stripslashes(json_encode($jsonRetorno, JSON_UNESCAPED_UNICODE));
            
            exit(); // PARO A EXECUÇÃO DO CÓDIGO
        }
        
        //LIMPO A VERIÁVEL         
        $post = $this->anti_injection($post);
        
        //PEGA O VETOR RETORNADO
        $getConfere = $this->cliente->dados_licenca($post);               
        
        //EXTRAIO O VALOR DO INDICE ZERO
        $jsonConfere = $getConfere[0];
        
        //RETORNO O VETOR
        return $this->view("cliente",stripslashes(json_encode($jsonConfere, JSON_UNESCAPED_UNICODE)));
                
    }
    
    public function confere_simples($id = NULL) {                
        
        //PEGO O ID ENVIADO E LIMPO
        $id = $this->anti_injection($id);
        
        //VERIFICO SE O ID NÃO ESTÁ VAZIO OU NULO
        if((is_null($id)) || ($id == '')){
            
            return "{'status':'NULL'}";
            exit(); // PARO A EXECUÇÃO DO CÓDIGO
        }
        
        //FAÇO A CONSULTA NA MODEL
        $getConfereSimples = $this->cliente->confere_simples($id);
        
        //PEGO O VETOR NO INDICE ZERO
        $jsonConfereSimples = $getConfereSimples[0];
        
        //RETORNO O RESULTADO
        return $this->view("cliente",stripslashes(json_encode($jsonConfereSimples, JSON_UNESCAPED_UNICODE)));
    }
    
    public function confere_lista($autoriza = null) {
        
        if($autoriza == true){
            
            $getLista = $this->cliente->confere_lista();
            
            return $this->view("cliente",stripslashes(json_encode($getLista, JSON_UNESCAPED_UNICODE)));
        }
        else{
            return json_encode(array("status"=>"acesso negado!"));
        }
        
    }
    
    public function listar() {
        
        $clientes = $this->cliente->listar();
        
        $this->authenticated_view("listar_clientes",$clientes); 
    }
    
    public function listar_todos() {
        
        $clientes = $this->cliente->listar();
        
        $this->authenticated_view("buscar_clientes",$clientes); 
    }
    
    public function listar_ativos() {
        
        if($this->cliente->autenticar_usuario()){
           
            $getAtivos = $this->cliente->listar_ativos();
            
            $this->authenticated_view("buscar_clientes",$getAtivos);
        }                
    }
    
    public function listar_vencendo() {
        
        if($this->cliente->autenticar_usuario()){
           
            $getVencendo = $this->cliente->listar_vencendo();
            
            $this->authenticated_view("buscar_clientes",$getVencendo);
        }                
    }
    
    public function listar_vencido() {
        
        if($this->cliente->autenticar_usuario()){
           
            $getVencido = $this->cliente->listar_vencido();
            
            $this->authenticated_view("buscar_clientes",$getVencido);
        }                
    }
    
    public function listar_bloqueado() {
        
        if($this->cliente->autenticar_usuario()){
           
            $getBloqueado = $this->cliente->listar_bloqueado();
            
            $this->authenticated_view("buscar_clientes",$getBloqueado);
        }                
    }
    
    public function buscar() {
        
        if((!empty($_POST)) && ($_POST['buscarCliente'])){                        
            
            //O BUSCA NOME E O BUSCA CNPJ PEGAM DA MESMA VARIÁVEL, MAS A BUSCA CNPJ GANHA A MÁSCARA
            $buscarNome = $this->anti_injection($_POST['buscarCliente']);
            
            $buscarCNPJ = $this->anti_injection($_POST['buscarCliente']);                        
            
            $filtroBusca = $this->anti_injection($_POST['filtroBusca']);
                       
            $clientes = $this->cliente->buscar_clientes($buscarNome,$buscarCNPJ,$filtroBusca);
            
            if($clientes){
                
                $this->authenticated_view("buscar_clientes",$clientes);
            }
            else{
                $this->authenticated_view("buscar_clientes","result_false");
            }
            
            
        }else{
            
            //SE A BUSCAR ESTIVER VAZIA ENTÃO VERIFICO O TIPO DE FILTRO MASCADO E RETORNO A LISTA
            $filtro = $this->anti_injection($_POST['filtroBusca']);
            
            if($filtro == "todos"){
                
                $getTodos = $this->cliente->listar();
                
                $this->authenticated_view("buscar_clientes",$getTodos);
            }
            if($filtro == "ativos"){
                
                $getAtivos = $this->cliente->listar_ativos();
                
                $this->authenticated_view("buscar_clientes",$getAtivos);
            }
            if($filtro == "vencendo"){
                
                $getVencendo = $this->cliente->listar_vencendo();
                
                $this->authenticated_view("buscar_clientes",$getVencendo);
            }
            if($filtro == "vencido"){
                
                $getVencido = $this->cliente->listar_vencido();
                
                $this->authenticated_view("buscar_clientes",$getVencido);
            }
            if($filtro == "bloqueado"){
                
                $getBloqueado = $this->cliente->listar_bloqueado();
                
                $this->authenticated_view("buscar_clientes",$getBloqueado);
            }
            
        }               
    }
    
    public function editar(){                
        
        @$idCliente = $this->getParams();
        
        if((isset($idCliente['idCliente'])) && ($idCliente['idCliente'] != '') ){
         
            $idCliente = $this->anti_injection($idCliente);
            
            $cliente = $this->cliente->buscar_cliente_editar($idCliente['idCliente']);                        
        
            $modulos = $this->cliente->listar_modulos();
            
            $this->authenticated_view("editar_cliente",$cliente,$modulos);
        }                
        elseif(!empty($_POST)){           
            
            if($this->cliente->autenticar_usuario()){                               
                
                $post = $this->anti_injection($_POST);  
                
                $dadosCliente['idCliente'] = $post['idCliente'];
                $dadosCliente['dtEdicao'] = $post['dtEdicao'];
                $dadosCliente['cnpj'] = $post['cnpj'];
                $dadosCliente['nmFantasia'] = $post['nmFantasia'];
                $dadosCliente['apelido'] = $post['apelido'];
                $dadosCliente['qtdDispositivos'] = $post['qtdDispositivos'];
                
                $modulos = $post;
                
                unset($modulos['idCliente']);
                unset($modulos['dtEdicao']);
                unset($modulos['cnpj']);
                unset($modulos['nmFantasia']);
                unset($modulos['apelido']);
                unset($modulos['qtdDispositivos']);
                
                if($this->cliente->consulta_cnpj_editar($dadosCliente['cnpj'], $dadosCliente['idCliente'])){
                    
                    header("location:".URL_BASE."cliente/editar/idCliente/".$dadosCliente['idCliente']."/?cnpj=true");
                    exit();
                }                                                                               
                
                if(!isset($_SESSION)){
                    
                    session_start();
                }
                
                //pego o id de quem editou
                $dadosCliente['nmEdicao'] = $_SESSION['idUsuario'];
                                
                //pego o retorno da edicao
                $editarCliente = $this->cliente->editar_cliente($dadosCliente,$modulos);                                                                 
                
                $modulos = $this->cliente->listar_modulos();
                                           
                $dadosClienteAtualizado = $this->cliente->buscar_cliente_editar($dadosCliente['idCliente']);
                
                $this->authenticated_view("editar_cliente", $dadosClienteAtualizado, $modulos, $editarCliente);
                
            }
            else{
                echo "acesso negado";
            }
        }        
    }   
    
    public function historico() {
        
        if($this->getParams()){
        
            $cliente = $this->getParams();
            
            if(isset($cliente['idCliente'])){
            
                $idCliente = $this->anti_injection($cliente['idCliente']);
                
                $getCliente = $this->cliente->buscar_cliente_relatorio($idCliente);
                
                // OBS: Os dias são calculados pela dtValidadeInicial - dtInsercao
                $getHistorico = $this->cliente->listar_historico($idCliente);
                
                $this->authenticated_view("historico", $getCliente, $getHistorico);
                                
            }
        }                
    }
    
    public function liberar() {                
        
        if(!empty($_POST)){
            
            if($this->cliente->autenticar_usuario()){
                
                $idCliente = $this->anti_injection($_POST['idCliente']);
                
                $setLiberar = $this->cliente->liberar_licenca($idCliente);
                
                if($setLiberar){
                    
                    echo "Licença Liberada";
                }
                else{
                    
                    $erro = $setLiberar;
                    
                    return $erro;
                }
            }
        }
    }
    
    public function dispositivos() {
        
        //PEGO O PARAMETRO
        $cliente = $this->getParams();
        //VERIFICO SE O PARAMETRO FOI PASSADO
        if(!empty($cliente))
        {
            
            if($this->cliente->autenticar_usuario())
            {
             
                if((isset($cliente['idCliente'])) && ( !empty($cliente['idCliente'])) && (!is_null($cliente['idCliente'])) ){
                
                    $idCliente = $this->anti_injection($cliente['idCliente']);
                    
                    $getDadosCliente = $this->cliente->buscar_cliente_relatorio($idCliente);

                    if(empty($getDadosCliente)){
                        echo "Cliente inexistente";
                        exit;
                    }
                    $dispositivos = $this->cliente->buscar_dispositivos($idCliente);                                                                                                     
                    
                    $getLimiteAtual = $this->cliente->verificarLimiteDispositivos($idCliente);
                    
                    $this->authenticated_view("dispositivos_cliente", $getDadosCliente,$dispositivos, $getLimiteAtual);
                                
                }
                else{
                    echo "parametro vazio";
                }
            }
            else{
                echo "acesso negado";
            }
        }
        else{
            echo "URL invalida";
        }
    }
    /**
     * @tutorial essa funçao deve retorna um arquivo json contendo status:boolean, chave:'string', cliente:'string'
     */
    public function chave_licenca(){
        
        if($this->cliente->autenticar_usuario()){
                    
            $dados = $this->anti_injection($_POST);  
            
            $nmCliente = $dados['nmClienteChaveManual'];
          
            $idCliente = $dados['idClienteChaveManual'];            
  
            $qtdDispositivos = $dados['qtdDispChaveManual'];            

            $diasLicenca =  $dados['diasChaveManual'];  
            
            $SO = $dados['so'];       
            
            $memoria = $dados['memoria'];  
            
            $serial = strtoupper($dados['bloco1'])."-".strtoupper($dados['bloco2'])."-".strtoupper($dados['bloco3'])."-".strtoupper($dados['bloco4'])."-".strtoupper($dados['bloco5']);
                                       
            $serialValidado = $this->validar_serial($serial);
            
            if($serialValidado['status'] == true){
                
                $blocosID = $serialValidado['serial'];
                
                $hwid1 = $blocosID[1][1];
                $hwid2 = $blocosID[2][1];
                $hwid3 = $blocosID[3][1];
                $chave = $this->gerar_chave_cliente($diasLicenca, $qtdDispositivos, $hwid1, $hwid2, $hwid3);                
                $status = true;               
                $msg = "";
                $this->cliente->registrar_historico_cliente($idCliente, "Chave Licença", $diasLicenca, "Chave de ativação manual: <b>{$chave}</b> - S.O: {$SO} - Memória: {$memoria}", NULL, $qtdDispositivos);
                $this->cliente->log_sistema("Chave de licença gerada: {$chave} para {$nmCliente} ID: {$idCliente}", "Chave Licença");
            }
            else{
                                
                $status = false;
                $chave = "Serial inválido";
                $msg = "Chave inválida";
                $this->cliente->log_sistema("Falha na validação do Serial: {$serial} do Cliente: {$nmCliente} ID: {$idCliente}", "Validar Serial");
            }
            
            echo json_encode(array("status"=>$status,"cliente"=>$nmCliente,"msg"=>$msg,"chave"=>$chave)) ;
        }
        
    }
    
    //bloco é o serial
    private function validar_serial($serial) {

        $blocosID = explode('-', $serial);
        $resultadoVerificacao = true;
        foreach ($blocosID as $bloco) {
            if ($this->cliente->verificaDigitoVerificador($bloco) == false) {
                $resultadoVerificacao = false;
            }
        }
        if ($resultadoVerificacao == true) {
                        
            return array("status"=>true,"serial"=>$blocosID);
        } 
        else {
            return false;
        }
    }
    
    private function gerar_chave_cliente($diasLicenca, $qtdDispositivos, $hwid1, $hwid2, $hwid3) {
                
        
        return $this->cliente->gerar_chave_licenca($diasLicenca, $qtdDispositivos, $hwid1, $hwid2, $hwid3);
    }
    
    public function validarCNPJ() {
        
        $cnpj = $this->anti_injection($_GET['cnpj']);
        
        if(!$this->cliente->validar_cnpj($cnpj)){
            echo 0;
        }
        else{
            echo 1;
        }
    }
        
}
