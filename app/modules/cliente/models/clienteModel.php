<?php

class clienteModel extends AppModel{
    
    public function autoriza($post) {
    
        @$idCliente       = $post['idCliente'];
        //@$tpPerfil        = $post['tpPerfil'];
        @$incrementoDias  = (int) $post['incrementoDias'];
        @$incrementoMeses = (int)$post['incrementoMeses'];
        @$dtAtualizacao   = $post['dtAtualizacao'];  //data em que está ocorrendo a atualização, não vem do banco
        @$tpStatus        = (int) $post['tpStatus'];
        @$nmUsuario       = $post['nmUsuario'];
        @$observacao      = isset($post['observacao']) ? $post['observacao'] : NULL;
        @$nmFantasia      = $post['nmFantasia'];  
        @$apelido         = $post['apelido'];  
        
        try {

            $getCliente = $this->read("*", "tbclientes", "idCliente = '{$idCliente}'");

            if($getCliente){
                
                $dtValidade = strtotime($getCliente[0]['dtValidade']); // $dtValidade veio do banco
                $dtAtualizacaoInt = (int)$dtAtualizacao;   //$dtAtualizaçãoInt veio do json
                
                if($incrementoMeses != 999){
                    
                    //QUANDO A DATA DE VALIDADE É MAIOR QUE A DATA DE ATUALIZAÇÃO, AINDA RESTAM DIAS DE USO DA LICENÇA.
                    if ($dtValidade > $dtAtualizacaoInt){
                        
                        $qryAutorizaCliente = "UPDATE tbclientes SET tpStatus = '{$tpStatus}', tpPerfil = '{$tpPerfil}', dtValidade = ADDDATE(dtValidade, INTERVAL {$incrementoMeses} MONTH), dtEdicao = FROM_UNIXTIME('{$dtAtualizacao}') WHERE idCliente = '{$idCliente}'"; 
                        
                        if(!$this->updateAdvanced($qryAutorizaCliente)){
                            return array("status"=>"Falha ao Autorizar a licença do cliente (tipo: Meses e dtValidade > dtAtualizacaoInt)");
                        }                        
                    } 
                    else{
                        
                        $qryAutorizaCliente = "UPDATE tbclientes SET tpStatus = '{$tpStatus}', tpPerfil = '{$tpPerfil}', dtValidade = ADDDATE(FROM_UNIXTIME('{$dtAtualizacao}'), INTERVAL {$incrementoMeses} MONTH), dtEdicao = FROM_UNIXTIME('{$dtAtualizacao}') WHERE idCliente = '{$idCliente}'"; 
                        
                        if(!$this->updateAdvanced($qryAutorizaCliente)){
                            return array("status"=>"Falha ao Autorizar a licença do cliente (tipo: Meses e dtValidade <= dtAtualizacaoInt)");
                        }
                    }
                    
                      //OBS: AO RENOVAR A LICENÇA, NÃO ALTERO MAIS O STATUS DOS DISPOSITIVOS, POIS POSSO TER MAIS DISPOSITIVOS QUE O LIMITE DA LICENÇA
//                    //autorizo as estações
//                    $qryAutorizaDisp = "UPDATE tbDispositivos SET statusDispositivo = '{$tpStatus}', dtValidade = (SELECT dtValidade FROM tbclientes where idCliente = '{$idCliente}') WHERE idCliente = '{$idCliente}'"; 
//                    
//                    if(!$this->updateAdvanced($qryAutorizaDisp)){
//                        
//                        return array("status"=>"Falha ao Autorizar a licença nos Dispositivos do cliente");
//                    }

                    //Empresa atualizada com sucesso
                    return array("status"=>"Cliente autorizado com sucesso!");
                }
                else{
                    
                    try {
                        
                    $this->beginTransaction();
                        
                    //INCREMENTO EM DIAS
                    if ($dtValidade > $dtAtualizacaoInt){
                        
                        $qryAutorizaCliente = "UPDATE tbclientes SET tpStatus = '{$tpStatus}', dtValidade = ADDDATE(dtValidade, INTERVAL {$incrementoDias} DAY), dtEdicao = FROM_UNIXTIME('{$dtAtualizacao}'), nmEdicao ='{$nmUsuario}' WHERE idCliente = '{$idCliente}'"; 
                        
                        if(!$this->updateAdvanced($qryAutorizaCliente)){
                            return array("status"=>"Falha ao Autorizar a licença do cliente (tipo: Meses e dtValidade > dtAtualizacaoInt)");
                        }                        
                    } 
                    else{
                        
                        $qryAutorizaCliente = "UPDATE tbclientes SET tpStatus = '{$tpStatus}', dtValidade = ADDDATE(FROM_UNIXTIME('{$dtAtualizacao}'), INTERVAL {$incrementoDias} DAY), dtEdicao = FROM_UNIXTIME('{$dtAtualizacao}'), nmEdicao ='{$nmUsuario}' WHERE idCliente = '{$idCliente}'"; 
                        
                        if(!$this->updateAdvanced($qryAutorizaCliente)){
                            return array("status"=>"Falha ao Autorizar a licença do cliente (tipo: Meses e dtValidade <= dtAtualizacaoInt)");
                        }
                    }                                       

                    if($incrementoDias>=1){
                        $tipo = "Renovação";
                        $mensagem = "Renovando a licença do cliente: {$nmFantasia}-{$apelido} ID: {$idCliente} por {$incrementoDias} dias";
                    }
                    else{
                        $tipo = "Correção";
                        $mensagem = "Corrigindo dias da licença do cliente: {$nmFantasia}-{$apelido} ID: {$idCliente} por {$incrementoDias} dias";
                    }
                    
                    $this->registrar_historico_cliente($idCliente, $tipo, $incrementoDias, $observacao);

                    $this->log_sistema($mensagem, $tipo);
                    
                    $this->commitTransaction();
                    
                    //Empresa atualizada com sucesso
                    return array("status"=>"Cliente autorizado com sucesso");
                    
                    } catch (PDOException $exc) {
                        
                        $this->rollbackTransaction();
                        echo $exc->getMessage();
                    }
                    
                }
            }
            else {
                return array("status"=>"Cliente não encontrado");
            }
        } catch (PDOException $exc) {
            
            return array("status" => "Ocorreu um erro: " . $exc->getMessage());
        }
    }
    
    public function confere_simples($id) {
        
        $colunas = "nmFantasia,tpStatus";
        $tabela  = "tbclientes";
        $where   = "idCliente = '{$id}'";
        
        $getCliente = $this->read($colunas, $tabela, $where);
        
        if($getCliente){
            
            return $getCliente;
        }
        else{
            
            $back[0] = array('status'=>'-1');
            return $back;
        }               
    }
    
    public function dados_licenca($post) {
        
        //AO SER SOLICITADO DADOS DA LICENÇA, VERIFICO SE O DISPOSITIVO JÁ ESTÁ ESTÁ CADATRADO PARA CHEGAR SE O DISPOSITIVO REALMENTE PERTENCE AO IDCLIENTE PASSADO
        $idCliente = $post['idCliente'];
        
        $idDispositivo = $post['idDispositivo'];
        
        $this->registrar_ultimo_acesso($idDispositivo);
        
        //CONTO SE ENCONTROU ALGUM DISPOSITIVO
        $buscarDispositivo = $this->readAdvanced("select count(*) buscarDispositivo,idCliente from tbdispositivos where idDispositivo= '{$idDispositivo}'");
        
        $achou = $buscarDispositivo[0]['buscarDispositivo'];
        
        //SE ENCONTRAR O DIPOSITIVO NA TBDISPOSITIVO, VERIFICO SE O IDCLIENTE PASSADO É O MESMO DO CADASTRO DO DISPOSITIVO
        if($achou == '1'){
            
            if($buscarDispositivo[0]['idCliente'] != $post['idCliente']){
                                
                $back[0] = array('status'=>-3,'msg'=>'O dispositivo não pode solicitar licença de um cliente que ele não está vinculado');
                return $back;
                exit;
            }
        }                
        
        $colunas = "idCliente, nmCliente, nmFantasia, tpPerfil, tpStatus, qtdDispositivos,date_format(dtInsercao, '%d/%m/%Y') as dtInsercao, "
                . "nmInsercao, date_format(dtEdicao, '%d/%m/%Y') as dtEdicao, nmEdicao, date_format(dtBloqueio, '%d/%m/%Y') as dtBloqueio, nmBloqueio,"
                . "date_format(dtValidade, '%d/%m/%Y') as dtValidade, date_format(dtEdicao, '%d/%m/%Y') as dtEdicao, chaveAtual, "
                . "date_format(dtChaveAtual, '%d/%m/%Y') as dtChaveAtual";
        $tabela  = "tbclientes";
        $where   = "idCliente = '{$post['idCliente']}'";

        $getCliente = $this->read($colunas, $tabela, $where);

        if($getCliente){

            
            return $getCliente;
        }
        else{
            $back[0] = array('status'=>-1,'msg'=>'Cliente nao encontrado');
            return $back;
        }
        
    }
    
    public function confere_lista() {
        
        return $this->read("idCliente, nmFantasia","tbclientes");
    }
        
    public function consulta_cnpj($cnpj) {
        
        if($this->read("cnpj", "tbclientes", "cnpj = '{$cnpj}'")){
            
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public function consulta_cnpj_editar($cnpj, $idCliente) {
        
        if($this->read("cnpj", "tbclientes", "cnpj = '{$cnpj}' and idCliente <> '{$idCliente}'")){
            
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public function cadastrar($post, $modulos,$idUsuario){
        
        try {
            
            //-------------------------------------- GERAR ID INTERNO -------------------------------------
            //pego 
            $proximoID = "select max(cast(idInterno as unsigned))+1 as proximoId from tbclientes;";           
            
            $idInterno = $this->readAdvanced($proximoID);
            
            //NA PRIMEIRA VEZ O ID É NULLO
            if(is_null($idInterno[0]['proximoId'])){
                
                $idInterno[0]['proximoId'] = '000001';                                
            }
            
            $post['idInterno'] = str_pad($idInterno[0]['proximoId'], 6, "0", STR_PAD_LEFT);

            //------------------------------------- GERAR ID CLIENTE -----------------------------------------
            
            //6 é o tamanho da string de retorno
            $idCliente = $this->criptografar_id(mt_rand(), 8);
            
            $post['idCliente'] = $idCliente;
            
            //-------------------------------------------------------------------------------------------------                                   
            
            $post['dtInsercao'] = date("Y-m-d H:i:s");
            
            $post['tpStatus'] = '1';
            
            $tabela = 'tbclientes';

            $this->beginTransaction();
            
            if ($this->insert($tabela, $post)) {                                

                //cadastra os modulos e retorna o nome dele                
                $nmModulos = $this->cadastrar_modulo_cliente($idCliente, $modulos, $idUsuario,$post['dtInsercao']);
                
                //GERO A QUANTIDADE DE DIAS GERADOS
                $dias = $this->diferenca_data($post['dtValidade'], date("Y-m-d"));
                
                //GRAVO O HISTÓRICO DA LICENÇA
                $this->registrar_historico_cliente($idCliente, "Licença Inicial", $dias, NULL, $nmModulos[0]['nmModulos'], $post['qtdDispositivos']);
                
                //GRAVO LOG DO SISTEMA
                $this->log_sistema("cadastrou um novo cliente: ".$post['nmFantasia']." - ".$post['apelido']." CNPJ: ".$post['cnpj']." - ID CLinte: ".$idCliente, "inserir");
                
                $this->commitTransaction();
                
                $result = array('status'=>'cadastro_true','idCliente'=>$idCliente);
                
                return $result;
            } else {
                
                $this->rollbackTransaction();
                
                $result = array('status'=>'cadastro_false');

                return $result;
            }
            //*********************************************************************************************  
        } catch (PDOException $exc) {
            
            return array("status"=>"Ocorreu falha no cadastro: ".$exc->getMessage());
        }
    }
    
    protected function cadastrar_modulo_cliente($idCliente, $modulos, $idUsuario, $data) {
                
        foreach($modulos as $modulo){
            
             $this->insertAdvance("insert into tbmodulocliente (idCliente,idModulo,idUsuarioInsercao,data) values ('{$idCliente}', '{$modulo}', '{$idUsuario}', '{$data}')");                          
        }
        
        return $this->readAdvanced("select group_concat(m.nmModulo) nmModulos from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) where mc.idCliente = '{$idCliente}'");
                
    }
    
    
    public function listar() {
        
        return $clientes = $this->read("idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj ,"
                . "qtdDispositivos,(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "date_format(dtValidade,'%d/%m/%Y') as dtValidade,"
                . "tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                . "where mc.idCliente = tbclientes.idCliente) as modulosCliente", "tbclientes", NULL , "nmFantasia");
    }
    
    public function listar_ativos() {
        
        return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes, "
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                . "from tbclientes where tpStatus = '1' and datediff(dtValidade,curdate())>=6 order by nmFantasia");
    }
    
    public function listar_vencendo() {
        
        return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                . "from tbclientes where tpStatus = '1' and datediff(dtValidade,curdate())<=5 and datediff(dtValidade,curdate())>=1 order by nmFantasia");
    }
    
    public function listar_vencido() {
        
        return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj,"
                . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                . " from tbclientes where tpStatus = '1' and datediff(dtValidade,curdate()) <= 0 order by nmFantasia");
    }
    
    public function listar_bloqueado() {
        
        return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj,"
                . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo)"
                . " where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                . " from tbclientes where tpStatus = '0' order by nmFantasia");
    }
    
           
    
    public function buscar_clientes($buscarNome,$buscarCNPJ,$filtroBusca = ''){
        
        if($filtroBusca == "todos"){
            
            return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj,"
                    . " qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                    . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                    . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                    . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                    . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                    . "from tbclientes where (nmFantasia like '%{$buscarNome}%') or (cnpj like '{$buscarCNPJ}%')");
        }
        if($filtroBusca == "ativos"){
            
            return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj,"
                    . " qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                    . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                    . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes, "
                    . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                    . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                    . "from tbclientes where (nmFantasia like '%{$buscarNome}%' and tpStatus = '1' and datediff(dtValidade,curdate())>=6) or ( cnpj like '{$buscarCNPJ}%' and tpStatus = '1' and datediff(dtValidade,curdate())>=6)");
        }
        if($filtroBusca == "vencendo"){
            
            return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                    . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                    . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                    . "date_format(dtValidade,'%d/%m/%Y') as dtValidade,tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                    . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                    . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                    . "from tbclientes where (nmFantasia like '%{$buscarNome}%' and tpStatus = '1' and datediff(dtValidade,curdate())<=5 and datediff(dtValidade,curdate())>=1) or (cnpj like '{$buscarCNPJ}%' and tpStatus = '1' and datediff(dtValidade,curdate())<=5 and datediff(dtValidade,curdate())>=1)");
        }
        if($filtroBusca == "vencido"){
            
            return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj,"
                    . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                    . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                    . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                    . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                    . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                    . " from tbclientes where (nmFantasia like '%{$buscarNome}%' and  tpStatus = '1' and datediff(dtValidade,curdate()) <= 0) or (cnpj like '{$buscarCNPJ}%' and  tpStatus = '1' and datediff(dtValidade,curdate()) <= 0)");
        }
        if($filtroBusca == "bloqueado"){
            
            return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                    . "qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente) as qtdDispCadastrados,"
                    . "(select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                    . "date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus, tpPerfil, datediff(dtValidade,curdate()) as diasRestantes,"
                    . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                    . "where mc.idCliente = tbclientes.idCliente) as modulosCliente "
                    . "from tbclientes where (nmFantasia like '%{$buscarNome}%' and tpStatus = '0') or (cnpj like '{$buscarCNPJ}%' and tpStatus = '0')");
        }
    }
          
    public function buscar_cliente_renovar($idCliente){
        
        return $this->readAdvanced("select idCliente, nmCliente, nmFantasia,apelido,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                . "qtdDispositivos,date_format(dtValidade,'%d/%m/%Y') as dtValidade, tpStatus,tpPerfil, datediff(dtValidade,curdate()) as diasRestantes "
                . "from tbclientes where idCliente = '{$idCliente}'");
        
    }
    
    public function buscar_cliente_editar($idCliente) {
        
        return $this->read("idCliente,nmFantasia, nmCliente, apelido,cnpj,qtdDispositivos, "
                . "(select group_concat(m.nmModulo) from tbmodulocliente mc inner join tbmodulo m on (mc.idModulo = m.idModulo) "
                . "where mc.idCliente = tbclientes.idCliente) modulosCliente","tbclientes", "idCliente = '{$idCliente}'");
    }
   
    
    public function buscar_cliente_relatorio($idCliente) {
        
        return $this->readAdvanced("select idCliente,nmFantasia, apelido,nmInsercao as responsavel ,MASK(cnpj, '##.###.###/####-##') AS cnpj, "
                . "tpStatus, qtdDispositivos, (select count(*) from tbdispositivos where idCliente = tbclientes.idCliente and statusDispositivo = 1) as qtdDispAtivos,"
                . "tpPerfil, date(dtValidade) dtValidade, "
                . "concat(date_format(dtInsercao,'%d/%m/%Y '), time(dtInsercao)) as dtInsercao, "
                . "date_format(dtInsercao,'%d/%m/%Y') as data from tbclientes where idCliente = '{$idCliente}'");
    }
    
    public function editar_cliente($post, $modulos) {
                        
        try {
            
            $this->beginTransaction();                                                
            
            $this->checar_mudanca_licenca($post['idCliente'], $post['qtdDispositivos'], $modulos);
            
            $this->update("tbclientes", $post, "idCliente = '{$post['idCliente']}'");                                         
                
            $this->delete("tbmodulocliente", "idCliente = '{$post['idCliente']}'");

            //Nessa caso o nmUsuario está contendo o id do usuario
            $this->cadastrar_modulo_cliente($post['idCliente'], $modulos, $post['nmEdicao'], $post['dtEdicao']); 

            $this->log_sistema("Edição do cliente: ".$post['nmFantasia']." - ".$post['apelido']." CNPJ: ".$post['cnpj'], "editar");                                                

            $this->commitTransaction();

            return "edicao_true";
            
                        
        } catch (PDOException $ex) {
            
            $this->rollbackTransaction();
            return $ex->getMessage();
        }
                
    }
    
    /**
     * @tutorial Função para gravar o histórico da licença do cliente
     * @param type $idCliente - é o id do cliente (obrigatório)
     * @param type $tipo - é o evento ocorrido (obrigatório)
     * @param type $dias - quantidade de dias ocorrida no evento (opcional)
     * @param type $observacao - observação informada pelo administrador (opcional)
     * @return type boolean
     */
    
    public function registrar_historico_cliente($idCliente, $tipo, $dias = NULL, $observacao = NULL, $perfil = NULL, $qtdDispositivos = NULL) {
        
        if(!isset($_SESSION)){
            
            session_start();
        }
        
        $idResponsavel = $_SESSION['idUsuario'];
        
        $registro = array(
            
            "dias" => $dias,
            "qtdDispositivos"=>$qtdDispositivos,
            "tpPerfil"=>$perfil,
            "tipo" => $tipo,
            "idResponsavelFk" => $idResponsavel,
            "idClienteFk"   => $idCliente,
            "observacao"    => $observacao,
            "dtInserido"    => date("Y-m-d H:i:s")
        );
        
        try {
        
             $this->insert("tbhistoricocliente", $registro);
             return true;
            
        } catch (PDOException $ex) {
            
            return $ex->getMessage();
        }
                               
    }
    
    public function listar_historico($idCliente) {
        
        return $this->readAdvanced("select h.dias,h.tipo,h.qtdDispositivos,h.tpPerfil, u.nmUsuario as resposavel,"
                . "h.observacao,date_format(dtInserido,'%d/%m/%Y') as data,concat(date_format(dtInserido,'%d/%m/%Y '), "
                . "time(dtInserido)) as data_hora from tbhistoricocliente h inner join tbusuarios u on (h.idResponsavelFk = u.idUsuario) "
                . "where h.idClienteFk = '{$idCliente}' order by dtInserido desc;");
        
    }
    
    private function criptografar_id($idCliente, $tamanho) {

        $novoID = strtoupper(md5($idCliente));
        
        $novoID = substr($novoID, 0, $tamanho);
        
        $achou = true;      
        
        while($achou == true){
            
            $acharId = $this->read("idCliente", "tbclientes", "idCliente = '{$novoID}'");
            
            if($acharId){
                //GERA UM NÚMERO ALEATORIO, ENCRIPTA EM SHA1 E DEPOIS MD5 E PEGO OS 6 PRIMEIROS DIGITOS DA STRING
                $novoID = strtoupper(md5(sha1(mt_rand())));
                
                $novoID = substr($novoID, 0, $tamanho);
            }
            else{
                $achou = false;
            }            
        }
        
        return $novoID;
    }
    
    public function liberar_licenca($idCliente) {
        
        try {
        
            $this->beginTransaction();
            
            $this->updateAdvanced("update tbclientes set tpStatus = 1 where idCliente = '{$idCliente}'");
            
            //$this->updateAdvanced("update tbdispositivos set statusDispositivo = 1 where idCliente = '{$idCliente}'");
            
            $this->registrar_historico_cliente($idCliente, "Liberado");
            
            $nmCliente = $this->read("nmFantasia,apelido", "tbclientes", "idCliente = '{$idCliente}'");
            
            $this->log_sistema("Liberou a licença do cliente: ".$nmCliente[0]['nmFantasia']." - ".$nmCliente[0]['apelido'], "Liberar");
            
            $this->commitTransaction();
            
            return TRUE;
            
        } catch (PDOException $ex) {
            
            $this->rollbackTransaction();
            
            return "Erro: ".$ex->getMessage();
        }
                
    }
    public function bloqueia($post){        
        
        $idCliente  = $post['idCliente'];
        //$perfil     = $post['perfil'];
        $dtBloqueio = strtotime($post['dtBloqueio']);
        $nmUsuario  = $post['nmUsuario']; 
        $observacao = $post['observacao'];
        
        //VERIFICO SE O CLIENTE EXISTE, SENÃO EXISTIR RETORNO A MENSAGEM E PARO O SCRIPT
        if(!$this->read("idCliente", "tbclientes", "idCliente = '{$idCliente}'")){
            
            return  array('status'=>'Cliente não encontrado');
            exit();
        }
        //SE ELE ENCONTRAR O CLIENTE PROSSEGUE O BLOQUEIO
        try {
            
            $this->beginTransaction();
            
            //TIREI O PERFIL DA QUERY
            $sqlBloqueiaCliente = "update tbclientes set tpStatus = 0 ,dtBloqueio = FROM_UNIXTIME('{$dtBloqueio}'), nmBloqueio='{$nmUsuario}' WHERE idCliente = '{$idCliente}'";

            if(!$this->updateAdvanced($sqlBloqueiaCliente)){

                return  array('status'=>'Falha ao bloquear cliente');
            }           

            $this->registrar_historico_cliente($idCliente, "Bloqueio",NULL, $observacao);
            
            $nmCliente = $this->read("nmFantasia,apelido", "tbclientes", "idCliente = '{$idCliente}'");
            
            $this->log_sistema("O usuario: ".$nmUsuario." bloqueou a licença do cliente: {$nmCliente[0]['nmFantasia']} - {$nmCliente[0]['apelido']}", "Bloqueio");
            
            $this->commitTransaction();
            
            //SE CHEGAR AQUI, OCORREU TUDO CERTO
            return array('status'=>'1');
            
        } catch (PDOException $ex) {
            
            $this->rollbackTransaction();
            return array('status'=>'Erro no processo de bloqueio: '.$ex->getMessage());
        }
                               
    }
    
    public function buscar_dispositivos($idCliente) {
        
        return $this->read("*,concat(date_format(dtUltimoAcesso,'%d/%m/%Y '), time(dtUltimoAcesso)) as dtAcesso", "tbdispositivos", "idCliente = '{$idCliente}'");
    }

    public function verificarLimiteDispositivos($idCliente) {
        
        //PEGO O LIMITE DE  DISPOSITIVOS DA LICENÇA
        $getCliente = $this->readAdvanced("select qtdDispositivos from tbclientes where idCliente = '{$idCliente}'");
        //SETO O LIMITE
        $limiteDispositivos = $getCliente[0]['qtdDispositivos'];
        
        //PEGO A QUANTIDADE DE DISPOSITIVOS ATIVOS DO CLIENTE
        $getDispositivos = $this->readAdvanced("select count(*) as ativos from tbdispositivos where idCliente = '{$idCliente}' and statusDispositivo = 1");
        //SETO A QUANTIDADE ATIVOS
        $qtdDispAtivos = $getDispositivos[0]['ativos'];
        
        return ($limiteDispositivos - $qtdDispAtivos);
        
    }
    
    //REGISTRA ULTIMO ACESSO DO DISPOSITIVO
    private function registrar_ultimo_acesso($idDispositivo) {        
                
        //converter epoch to date
        $dtUltimoAcesso = date("Y-m-d H:i:s");
        
        if($this->update("tbdispositivos", array("dtUltimoAcesso"=>$dtUltimoAcesso), "idDispositivo = '{$idDispositivo}'")){

            return array("status"=>1);
        }
        else{
            return array("status"=>0);
        }
        
    }   
    
    //bloco é o serial passado pelo usuário.
    public function verificaDigitoVerificador($bloco) {

        if (strlen($bloco) > 1) {
            $Soma1 = 0;
            $Soma2 = 0;
            $str = '';
            for ($i = 0; $i <= strlen($bloco) - 2; $i++) {
                $Soma1 = $Soma1 + ord($bloco[$i]);
                $str = $str . $bloco[$i];
                if ((($i + 1) % 2) == 0) {
                    $Soma2 = $Soma2 + ord($bloco[$i]);
                }
            }
            $digitoVerificador = chr((($Soma1 + $Soma2) % 26) + 65);
            $digitoVerificadorBloco = $bloco[strlen($bloco) - 1];
            if ($digitoVerificador == $digitoVerificadorBloco) {
                return true;
//      return ' ' . $str . ' -> '. $digitoVerificador . $digitoVerificadorBloco;
            } else {
                           
                return false;
//      return ' ' . $str . ' -> '. $digitoVerificador . $digitoVerificadorBloco;
            }
        } else 
            {
            return false;
        }
    }
    
    public function gerar_chave_licenca($diasChave, $qtdDispositivos, $hwid1, $hwid2, $hwid3) {             
    
        //$codificador = new Base32;
        $bloco1 = $this->encode(':;' . $hwid1, False);
        $bloco2 = $this->encode($hwid1 . $diasChave, False);
        $bloco3 = $this->encode($hwid2 . $qtdDispositivos, False);
        $bloco4 = $this->encode('?!' . $hwid2,  False);
        //$bloco5 = $codificador->encode(time() / 60 % 60, False);
        $bloco5 = $this->encode($hwid3 . '@#', False);
        $bloco1 = $this->adicionaDigitoVerificador($bloco1);
        $bloco2 = $this->adicionaDigitoVerificador($bloco2);
        $bloco3 = $this->adicionaDigitoVerificador($bloco3);
        $bloco4 = $this->adicionaDigitoVerificador($bloco4);
        $bloco5 = $this->adicionaDigitoVerificador($bloco5);
        return $bloco1 . '-' . $bloco2 . '-' . $bloco3 . '-' . $bloco4 . '-' . $bloco5;
        
    }

    public function validar_cnpj($cnpj) {
        
        // Valida tamanho
	if (strlen($cnpj) != 14)
		return false;
	// Valida primeiro dígito verificador
	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
		return false;
	// Valida segundo dígito verificador
	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
        
	return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
    
    private function checar_mudanca_licenca($idCliente, $qtdDispositivos, $modulos) {
     
        
        $modulosEdicao = "";
        
        //concateno os modulos vindos do formulário
        foreach($modulos as $modulo){
        
            $modulosEdicao .= $modulo.",";
        }
        //removo a virgula do final
        $tamanho = strlen($modulosEdicao);       
	$idModulos = substr($modulosEdicao,0,$tamanho-1);                
        
        //primeiro pego os módulos viculados ao cliente atualmente
        $modulosCadastrados = $this->readAdvanced("select group_concat(idModulo) modulos from tbmodulocliente where idCliente = '{$idCliente}'");
        $qtdDispCadastrado = $this->readAdvanced("select qtdDispositivos from tbclientes where idCliente = '{$idCliente}'");             
               
        if(($qtdDispositivos != $qtdDispCadastrado[0]['qtdDispositivos']) && ($idModulos != $modulosCadastrados[0]['modulos']) ){
            
            $modulosCliente = $this->readAdvanced("select group_concat(nmModulo) nmModulos from tbmodulo where idModulo in ({$idModulos})");
            
            $this->registrar_historico_cliente($idCliente, "Licença Alterada", NULL, "Os módulos de uso e a quantidade de dispositivos foram modificados", $modulosCliente[0]['nmModulos'], $qtdDispositivos);
        }      
        elseif($qtdDispositivos != $qtdDispCadastrado[0]['qtdDispositivos'])
        {           
            
            $modulosCliente = $this->readAdvanced("select group_concat(nmModulo) nmModulos from tbmodulo where idModulo in ({$idModulos})");
            
            $this->registrar_historico_cliente($idCliente, "Licença Alterada", NULL, "Quantidade de dispositivos foi alterada.", $modulosCliente[0]['nmModulos'], $qtdDispositivos);
        }
        elseif($idModulos != $modulosCadastrados[0]['modulos']){
           
            $modulosCliente = $this->readAdvanced("select group_concat(nmModulo) nmModulos from tbmodulo where idModulo in ({$idModulos})");
            
            $this->registrar_historico_cliente($idCliente, "Licença Alterada", NULL, "Os módulos de uso foram modificados.", $modulosCliente[0]['nmModulos'], $qtdDispositivos);
        }
    }
    
    public function listar_modulos(){
        
        return $this->read("idModulo, nmModulo", "tbmodulo","status = 1");
    }
        
}

