<?php

/*
 * TODAS OS METODOS QUE TIVEREM MAIS DE UM NOME DEVEM SER SEPARADOS POR UNDERLINE '_'
 */


class index extends AppController
{

    private $index;

    public function __construct()
    {

        $this->index = new indexModel();

        parent::__construct();
    }

    public function index_action()
    {

        $this->view("home");
    }

    public function logar()
    {

        if ((isset($_POST['login'])) && (isset($_POST['senha'])) && ($_POST['login'] != '')) {

            $post = $_POST;

            $post = $this->anti_injection($post);

            $getLogar = $this->index->logar($post);

            if ($getLogar == "login_true") {

                header("location:" . URL_BASE . "admin");
            } else {
                $this->view("login", $getLogar);
            }
        } else {
            $this->view("login", "login_false");
        }
    }

    public function logout()
    {

        $this->index->logout();

        header("location:" . URL_BASE);
    }
}
