<?php


class indexModel extends AppModel{
    
    public function logar($post) {
        
        $senhaCrypt = md5($post['senha']).SALT;
        
        $getUsuario = $this->read("login,nmUsuario,token,idUsuario", "tbusuarios", "login = '{$post['login']}' and senha = '{$senhaCrypt}'");
        
        if($getUsuario){
            
            //SE NÃO EXISTIR SESSÃO CRIA UMA
            if(!isset($_SESSION)){
                
                session_start();
            }
            
            //CRIO VARIÁVEIS DE SESSÃO
            $_SESSION['nmUsuario'] = $getUsuario[0]['nmUsuario'];
            $_SESSION['token']     = $getUsuario[0]['token'];
            $_SESSION['idUsuario'] = $getUsuario[0]['idUsuario'];
            
            
            $idUsuario = $getUsuario[0]['idUsuario'];                       
            
            $data = date("Y-m-d H:i:s");
            
            $this->updateAdvanced("update tbusuarios set dtUltimoAcesso = '{$data}' where idUsuario = '{$idUsuario}'");                        
                        
            $registrarLogin = array(
                'idUsuario'=>$getUsuario[0]['idUsuario'], 
                'login'=>$getUsuario[0]['login'], 
                'nmUsuario'=>$getUsuario[0]['nmUsuario'],                 
                'token'=>$getUsuario[0]['token']);                              
            
            $this->registrar_login_usuario($registrarLogin);
            
            return "login_true";
        }
        else{
            return "login_false";
        }
    }   
      
    /**
     * 
     * @param array $dados conforme tabela tblogusuario
     * @tutorial Enviar vetor com array('idUsuario'=>'', 'login'=>'', 'nmUsuario'=>'', 'token'=>'')
     */
    
    private function registrar_login_usuario(array $dados) {
        
        $achou = FALSE;
        $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","Windows","Linux","MacOS","Mac");
        $so = "não identificado";
        foreach($user_agents as $user_agent){
            if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
                $achou = TRUE;
                $so    = $user_agent;
                break;
            }
        }
        
        //RENOVO O ID DA SESSAO ANTES DE SALVAR NO BANCO
        session_regenerate_id();
        
        $dados['idSessao'] = session_id();          
        $dados['IPAcesso'] = addslashes($_SERVER['REMOTE_ADDR']);                       
        $logSistema['data'] = date("Y-m-d H:i:s");
        $logSistema['idUsuario'] = $dados['idUsuario'];
        $logSistema['msg'] = "O usuário: {$dados['nmUsuario']} - ID: {$dados['idUsuario']} - IP: {$dados['IPAcesso']} - SO: {$so} iniciou a sessão: {$dados['idSessao']}";
        $logSistema['operacao'] = "Login";                              
        
        $this->insert("tblogsistema", $logSistema);
    }
    
    public function logout() {                    
        
        if(!isset($_SESSION)){
            session_start();
        }
        
        $this->registrar_encerramento();    
        
        unset($_SESSION['token']);
        unset($_SESSION['nmUsuario']);
        unset($_SESSION['idUsuario']);                
        
        session_unset($_SESSION);
        
        session_destroy();                
    }   
        
    private function registrar_encerramento(){
        
        $dados['data'] = date("Y-m-d H:s:i");
        $dados['msg'] = "O usuario: {$_SESSION['nmUsuario']} - ID: {$_SESSION['idUsuario']} encerrou a sessao: ". session_id();
        $dados['idUsuario'] = $_SESSION['idUsuario'];
        $dados['operacao'] = 'Logout';
        $this->insert("tblogsistema", $dados);
    }
 
}
