<?php

class sistemaModel extends AppModel{
    
    /**
     * @tutorial lista os logs de sistema armazenados     
     */
    public function logs() {
        
        $logs = $this->readAdvanced("select data,msg, nmUsuario,operacao from tblogsistema l inner join tbusuarios u on l.idUsuario = u.idUsuario order by idLogSistema desc");
        
        $usuarios = $this->readAdvanced("select distinct l.idUsuario,nmUsuario from tblogsistema l inner join tbusuarios u on l.idUsuario = u.idUsuario order by nmUsuario");
    
        $operacao = $this->readAdvanced("select distinct operacao from tblogsistema order by operacao");
        
        return array($logs,$usuarios,$operacao);
    }
    
    public function usuarios_filtro() {
        
        return $this->readAdvanced("select distinct l.idUsuario,nmUsuario from tblogsistema l inner join tbusuarios u on l.idUsuario = u.idUsuario order by nmUsuario");
    }
    
    public function operacao_filtro() {
        
        return $this->readAdvanced("select distinct operacao from tblogsistema order by operacao");
    }
    
    public function filtrar_log($data,$idUsuario,$operacao) {
       
        if($data != ""){
            
            $filtrarData = "date(data) = '{$data}'";
        }
        else{
            $filtrarData = $data;
        }
        
        if($idUsuario != ""){
            
            if($data == ""){
                
                $filtrarIdUsuario = "l.idUsuario = '{$idUsuario}'";
            }
            else{
                $filtrarIdUsuario = "and l.idUsuario = '{$idUsuario}'";
            }
        }
        else{
            $filtrarIdUsuario = $idUsuario;
        }
        
        if($operacao != ""){
            
            if(($data == "") || ($operacao == "") ){
                
                $filtrarOperacao = "operacao = '{$operacao}'";
            }
            else{
                $filtrarOperacao = "and operacao = '{$operacao}'";
            }
        }
        else{
            $filtrarOperacao = $operacao;
        }
        
        if( ($data != '') || ($idUsuario != '') || ($operacao != '') ){
            
            $condicao = "where";
        }
        else {
            $condicao = "";
        }
        
        
        $sql = "select data,msg, nmUsuario,operacao from tblogsistema l inner join tbusuarios u on (l.idUsuario = u.idUsuario)
                $condicao {$filtrarData} {$filtrarIdUsuario} {$filtrarOperacao}
                order by idLogSistema desc";
                
        return $this->readAdvanced($sql);        
    }
    
    public function listar_modudos() {
        
        return $this->read("*", "tbmodulo");
    }
    
    public function cadastrar_modulo($post) {
        
        if($this->insert("tbmodulo", $post)){
            
            $this->log_sistema("Novo módulo cadastrado: {$post['nmModulo']}", "Inserir");
            
            return true;
        }
        else{
            return false;
        }
    }
    
    public function buscar_modulo($nome) {
        
        if($this->read("nmModulo","tbmodulo","nmModulo = '{$nome}'")){
            
            return true;
        }
        else{
            return false;
        }
    }
    
    public function buscar_modulo_editar($id) {
        
        return $this->read("*", "tbmodulo", "idModulo = '{$id}'");
    }
    
    public function editar_modulo($modulo) {                        
        
        if($this->update("tbmodulo", $modulo, "idModulo = '{$modulo['idModulo']}'")){
            
            $this->log_sistema("O modulo: {$modulo['nmModulo']} - ID: {$modulo['idModulo']} foi editado", "editar");
            
            return "edicao_true";
        }
        else{
            return "edicao_false";
        }
    }
    
    public function verificar_nome_modulo($nmModulo, $idModulo) {
                        
        if($this->read("*", "tbmodulo", "nmModulo = '{$nmModulo}' and idModulo != '{$idModulo}'")){
            
            return true;
        }
        else{
            return false;
        }
    }
            
}

