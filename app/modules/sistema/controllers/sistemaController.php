<?php


class sistema extends AppController {
    
    private $sistema;
    
    public function __construct() {
        
        $this->sistema = new sistemaModel();
        
        parent::__construct();
        
    }                           
    
    public function logs() {
        
        if(isset($_GET['filtrar'])){                        
            
            $filtro = $this->anti_injection($_GET);
            
            $data      = $filtro['data'];
            $idUsuario = $filtro['usuario'];
            $operacao  = $filtro['operacao'];                        
            
            $resultadoFiltro = $this->sistema->filtrar_log($data, $idUsuario, $operacao);
            
            $usuario = $this->sistema->usuarios_filtro();
            
            $operacao = $this->sistema->operacao_filtro();
            
            $this->authenticated_view("logs", $resultadoFiltro, $usuario,$operacao);
        }
        else{
            
            $logs = $this->sistema->logs();
            
            $this->authenticated_view("logs", $logs[0], $logs[1],$logs[2]);                        
        }    
                            
    }
    
    public function listar_modulos() {
        
        $modulos = $this->sistema->listar_modudos();
        
        $this->authenticated_view("listar_modulos",$modulos);
    }
    
    public function cadastrar_modulo() {
        
        if(!empty($_POST)){
            
            if ((!empty($_POST['nmModulo'])) && (!empty($_POST['status']))) {

                if (!isset($_SESSION)) {
                    session_start();
                }

                $dados = $this->anti_injection($_POST);

                $dados['dtInsercao'] = date("Y-m-d H:i:s");

                $dados['idUsuarioInsercao'] = $_SESSION['idUsuario'];

                //VERIFICA SE O MÓDULO JÁ EXISTE
                if ($this->buscar_modulo($dados['nmModulo'])) {

                    $this->authenticated_view("cadastrar_modulo", "modulo_existe");
                    exit();
                }

                if ($this->sistema->cadastrar_modulo($dados)) {

                    $this->authenticated_view("cadastrar_modulo", "cadastro_true");
                } else {
                    $this->authenticated_view("cadastrar_modulo", "cadastro_false");
                }
            } 
            else{
                echo "Preencha o nome do módulo";
            }            
                        
        }
        else{
            $this->authenticated_view("cadastrar_modulo");
        }
    }
    
    public function buscar_modulo($nomeModulo = NULL){
        
        if(!is_null($nomeModulo)){
            
            return $this->sistema->buscar_modulo($nomeModulo);
        }
    }
    
    public function editar_modulo() {
        
        if($this->getParams()){
        
            $id = $this->anti_injection($this->getParams());
            
             $modulo = $this->sistema->buscar_modulo_editar($id['idModulo']);
            
            $this->authenticated_view("editar_modulo",$modulo);
        }
        else{
            if(isset($_POST)){                                
                
                $dadosModulo = $this->anti_injection($_POST);
                
                if($this->sistema->verificar_nome_modulo($dadosModulo['nmModulo'], $dadosModulo['idModulo'])){                                        
                    
                    $modulo = $this->sistema->buscar_modulo_editar($dadosModulo['idModulo']);
                    
                    $this->authenticated_view("editar_modulo",$modulo,"modulo_existe");
                }
                else{
                    
                    $editar = $this->sistema->editar_modulo($dadosModulo);

                    $modulo = $this->sistema->buscar_modulo_editar($dadosModulo['idModulo']);

                    $this->authenticated_view("editar_modulo",$modulo,$editar);  
                }
            }
        }
                
    }
}
