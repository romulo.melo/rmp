<?php

class AppModel {

    protected $db = null;
    private $_tabela;

    /**
     * @author Rômulo Melo
     * @access Protegido
     * @param String $tabela É necessário informar a tabela que será usada no DB
     */
//    function __construct() {
//
//        try {
//            if ($this->db == NULL) {
//                $opcoes = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
//
//                $this->db = new PDO("mysql:host=" . HOST_DB . ";dbname=" . DB_NAME, USER_NAME_DB, PWD_DB, $opcoes);
//            }
//        } catch (Exception $exc) {
//            echo "Falha na conexao: " . $exc->getMessage();
//        }
//    }

    protected function beginTransaction() {

        $this->db->beginTransaction();
    }

    protected function commitTransaction() {

        $this->db->commit();
    }

    protected function rollbackTransaction() {

        $this->db->rollBack();
    }

    protected function autenticar_dispositivo($idDispositivo, $idCliente, $token) {

        //CHECAR SE O TOKEN EXISTE
        //CHECAR SE O TOKEN REALMENTE PERTENCE AO DISPOSITIVO SOLICITANTE
        //CHECAR SE O DISPOSITIVO PERTENCE AO CLIENTE INFORMADO
    }

    /**
     * @author Rômulo Melo
     * @param array $dados Informe as colunas e valores a ser inseridos na tabela
     *
     */
    protected function insert($tabela, array $dados) {

        $this->_tabela = $tabela;

        foreach ($dados as $inds => $vals) {
            $campos[] = $inds;
            $valores[] = $vals;
        }

        $campos = implode(", ", $campos);
        $valores = "'" . implode("', '", $valores) . "'";

        $sql = "insert into `{$this->_tabela}` ({$campos}) values ({$valores})";

        return $this->db->query($sql);
    }

    protected function insertAdvance($sql) {

        return $this->db->query($sql);
    }

    /**
     *
     * @param type $tabela onde ler
     * @param type $where para um ler registro específico
     * @return vetor de dados
     */
    protected function read($colunas = NULL, $tabela, $where = NULL, $ordem = NULL) {

        $this->_tabela = $tabela;

        $colunas = ($colunas != NULL ? $colunas : "*");

        $where = ($where != NULL ? "WHERE {$where}" : "");

        $ordem = ($ordem != NULL ? "ORDER BY {$ordem}" : "");

        $q = $this->db->query("SELECT {$colunas} FROM {$this->_tabela} {$where} {$ordem}");

        $q->setFetchMode(PDO::FETCH_ASSOC);

        return $q->fetchAll();
    }

    protected function readAdvanced($sql) {

        $query = $this->db->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }

    /**
     *
     * @param type $colunas informe as colunas da tabela ex: "nome,telefone" - parametro opcional
     * @param type $tabela tabela quera será usada - parametro obrigratorio
     * @param type $like parametro de comparação ex: nome like '%fulano%'
     * @return type
     */
    protected function search($colunas = NULL, $tabela, $like) {

        $colunas = ($colunas != NULL ? $colunas : "*");

        $this->_tabela = $tabela;

        $search = $this->db->query("SELECT {$colunas} FROM {$this->_tabela} where {$like}");

        $search->setFetchMode(PDO::FETCH_ASSOC);

        return $search->fetchAll();
    }

    /**
     *
     * @param type $tabela tabela onde irá atualizaar
     * @param array $dados dados para atualizar
     * @param type $where id do registro para atualizar
     * @return type retorna boolean
     */
    protected function update($tabela, array $dados, $where) {

        $this->_tabela = $tabela;

        foreach ($dados as $ind => $val) {

            $campos[] = $ind . "= '" . $val . "'";
        }

        $campos = implode(", ", $campos);

        $update = $this->db->query("UPDATE `{$this->_tabela}` SET {$campos} where {$where}");

        return $update;
    }

    protected function updateAdvanced($sql) {

        $update = $this->db->query($sql);

        return $update;
    }

    /**
     *
     * @param type $where informe o ID do registro a ser excluíd
     * @param type $tabela Tabela onde será excluído o registro
     * @return type
     */
    protected function delete($tabela, $where = NULL) {

        $this->_tabela = $tabela;

        return $this->db->query("DELETE FROM {$this->_tabela} WHERE {$where}");
    }

    protected function sendMail($destinatario, $assunto, $conteudo) {


        $sendMail = new PHPMailer();

        $sendMail->isSMTP();

        $sendMail->Host = "mail.sistemasmais.net";

        $sendMail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)

        $sendMail->Username = 'comercial@sistemasmais.net'; // Usuário do servidor SMTP

        $sendMail->Password = 'C0mercial'; // Senha do servidor SMTP

        $sendMail->Port = 465;

        $sendMail->SMTPSecure = "ssl";

        $sendMail->From = "comercial@sistemasmais.net";

        $sendMail->FromName = "";

        $sendMail->addAddress($destinatario);

        $sendMail->isHTML();

        $sendMail->CharSet = "UTF-8";

        $sendMail->Subject = $assunto;

        $sendMail->Body = $conteudo;

        if ($sendMail->send()) {
            return TRUE;
        } else {
            return $sendMail->ErrorInfo;
        }
    }

    protected function anti_injection($var) {

        $var = filter_var($var, FILTER_SANITIZE_STRING);
        $var = filter_var($var, FILTER_SANITIZE_MAGIC_QUOTES);
        $var = filter_var($var, FILTER_SANITIZE_SPECIAL_CHARS);

        return $var;
    }

    /**
     * @tutorial função para paginação de resultados
     *
     */
    protected function pagination($busca, $exibir_a_partir_de, $quanditade_por_pagina) {

        $i = $exibir_a_partir_de;

        for ($i; $i <= $quanditade_por_pagina; $i++) {

            $busca_paginada[] = $busca[$i];
        }

        return $busca_paginada;
    }

    protected function upload(array $arquivo) {

        $destino = "app/uploads/";
        $ext = strtolower(substr($arquivo['name'], -4));
        $novo_nome = md5(time()) . $ext;
        if (move_uploaded_file($arquivo['tmp_name'], $destino . $novo_nome)) {

            $retorno['status'] = TRUE;
            $retorno['nome'] = $novo_nome;

            return $retorno;
        } else {
            return FALSE;
        }
    }

    protected function validar_json() {

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                echo 'No errors';
                break;
            case JSON_ERROR_DEPTH:
                echo 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                echo 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                echo 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                break;
        }
    }

    /**
     * @tutorial Verifica se o usuario esta logado
     */
    public function autenticar_usuario() {

        if (!isset($_SESSION)) {
            session_start();
        }

        if ((isset($_SESSION['nmUsuario'])) && (isset($_SESSION['token'])) && (isset($_SESSION['idUsuario']))) {

            $idUsuario = $_SESSION['idUsuario'];
            $nmUsuario = $_SESSION['nmUsuario'];
            $token = $_SESSION['token'];

            if ($this->read("idUsuario", "tbusuarios", "idUsuario = '{$idUsuario}' and nmUsuario = '{$nmUsuario}' and token = '{$token}'")) {

                return TRUE;
            } else {

                session_destroy();
                return FALSE;
            }
        } else {

            return FALSE;
        }
    }

    public function log_sistema($mensagem, $operacao) {

        if (!isset($_SESSION)) {
            session_start();
        }

        $dados['data'] = date("Y-m-d H:i:s");
        $dados['msg'] = $mensagem;
        $dados['idUsuario'] = $_SESSION['idUsuario'];
        $dados['operacao'] = $operacao;

        $tabela = "tblogsistema";

        $this->insert($tabela, $dados);
    }

    public function diferenca_data($data1, $data2) {

        $date1 = date_create($data1);
        $date2 = date_create($data2);
        $diff = date_diff($date1, $date2);
        return $diff->format("%a");
    }

//--------------------------CRIPTOGRAFIA DA CHAVE ---------------------------------------------
    private static $map = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '='  // padding char
    );
    private static $flippedMap = array(
        'A' => '0', 'B' => '1', 'C' => '2', 'D' => '3', 'E' => '4', 'F' => '5', 'G' => '6', 'H' => '7',
        'I' => '8', 'J' => '9', 'K' => '10', 'L' => '11', 'M' => '12', 'N' => '13', 'O' => '14', 'P' => '15',
        'Q' => '16', 'R' => '17', 'S' => '18', 'T' => '19', 'U' => '20', 'V' => '21', 'W' => '22', 'X' => '23',
        'Y' => '24', 'Z' => '25', '2' => '26', '3' => '27', '4' => '28', '5' => '29', '6' => '30', '7' => '31'
    );

    public static function encode($input, $padding = true) {
        if (empty($input))
            return "";

        $input = str_split($input);
        $binaryString = "";

        for ($i = 0; $i < count($input); $i++) {
            $binaryString .= str_pad(base_convert(ord($input[$i]), 10, 2), 8, '0', STR_PAD_LEFT);
        }

        $fiveBitBinaryArray = str_split($binaryString, 5);
        $base32 = "";
        $i = 0;

        while ($i < count($fiveBitBinaryArray)) {
            $base32 .= self::$map[base_convert(str_pad($fiveBitBinaryArray[$i], 5, '0'), 2, 10)];
            $i++;
        }

        if ($padding && ($x = strlen($binaryString) % 40) != 0) {
            if ($x == 8)
                $base32 .= str_repeat(self::$map[32], 6);
            else if ($x == 16)
                $base32 .= str_repeat(self::$map[32], 4);
            else if ($x == 24)
                $base32 .= str_repeat(self::$map[32], 3);
            else if ($x == 32)
                $base32 .= self::$map[32];
        }

        return $base32;
    }

    public static function decode($input) {
        if (empty($input))
            return;
        $paddingCharCount = substr_count($input, self::$map[32]);
        $allowedValues = array(6, 4, 3, 1, 0);
        if (!in_array($paddingCharCount, $allowedValues))
            return false;
        for ($i = 0; $i < 4; $i++) {
            if ($paddingCharCount == $allowedValues[$i] &&
                    substr($input, -($allowedValues[$i])) != str_repeat(self::$map[32], $allowedValues[$i]))
                return false;
        }
        $input = str_replace('=', '', $input);
        $input = str_split($input);
        $binaryString = "";
        for ($i = 0; $i < count($input); $i = $i + 8) {
            $x = "";
            if (!in_array($input[$i], self::$map))
                return false;
            for ($j = 0; $j < 8; $j++) {
                $x .= str_pad(base_convert(@self::$flippedMap[@$input[$i + $j]], 10, 2), 5, '0', STR_PAD_LEFT);
            }
            $eightBits = str_split($x, 8);
            for ($z = 0; $z < count($eightBits); $z++) {
                $binaryString .= ( ($y = chr(base_convert($eightBits[$z], 2, 10))) || ord($y) == 48 ) ? $y : "";
            }
        }
        return $binaryString;
    }

    public static function adicionaDigitoVerificador($bloco) {
        if (strlen($bloco) > 1) {
            $Soma1 = 0;
            $Soma2 = 0;
            $str = '';
            for ($i = 0; $i <= strlen($bloco) - 1; $i++) {
                $Soma1 = $Soma1 + ord($bloco[$i]);
                $str = $str . $bloco[$i];
                if ((($i + 1) % 2) == 0) {
                    $Soma2 = $Soma2 + ord($bloco[$i]);
                }
            }
            $digitoVerificador = chr((($Soma1 + $Soma2) % 26) + 65);
            $digitoVerificadorBloco = $bloco[strlen($bloco) - 1];
            return $str . $digitoVerificador;
        } else {
            return $bloco + '0';
        }
    }

    //-----------------------------------------------------------------------------
}
