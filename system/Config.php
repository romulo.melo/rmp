<?php

//AMBIENTE DE DESENVOLVIMENTO
date_default_timezone_set("America/Manaus");
header("Content-type: text/html; charset=utf-8");

//  AMBIENTE LOCAL
//
define('MODULES', 'app/modules/');
define('VIEWS', 'app/modules/');
define('DIR_STYLE', 'http://localhost/mais/style/');
define('DIR_STYLE_APP', 'http://localhost/mais/app/style/');
define('DIR_JS', 'http://localhost/mais/js/');
define('DIR_JS_APP', 'http://localhost/mais/app/js/');
define('URL_BASE', 'http://localhost/mais/');
define('DIR_TEMPLATE', "http://localhost/mais/app/template/");
define('SALT', '987t5cbd5db16ca46bac6bb95e@07002!');
define('HOST_DB', 'localhost');
define('USER_NAME_DB', 'root');
define('PWD_DB', '');
define('DB_NAME', 'cms');


//   AMBIENTE DE PRODUÇÃO
//
//    define('MODULES', 'app/modules/');
//    define('VIEWS', 'app/modules/');
//    define('DIR_STYLE','http://www.sistemasmais.net/registro/style/');
//    define('DIR_STYLE_APP','http://www.sistemasmais.net/registro/app/style/');
//    define('DIR_JS','http://www.sistemasmais.net/registro/js/');
//    define('DIR_JS_APP','http://www.sistemasmais.net/registro/app/js/');
//    define('URL_BASE','http://www.sistemasmais.net/registro/');
//    define('DIR_TEMPLATE', "http://www.sistemasmais.net/registro/app/template/");
//    define('SALT', '3ccb5cbd5db16ca46bac6bb95ea07282');
//    define('HOST_DB', 'localhost');
//    define('USER_NAME_DB', 'siste189_admweb');
//    define('PWD_DB', 's1t3m@sm@1w3B!!!');
//    define('DB_NAME', 'siste189_sistemasmais');





