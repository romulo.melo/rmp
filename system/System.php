<?php

class System{
   
    private $_url;
    private $_explode;
    private $_controller;
    private $_action;
    private $_params;
    
    public function __construct() {
        $this->setUrl();
        $this->setExplode();
        $this->setController();
        $this->setAction();
        $this->setParams();
    }
    
    private function setUrl() {
        
        //SE A URL VIER VAZIA ENTAO RECEBERA INDEX/INDEX_ACTION
        $_GET['url'] = (isset($_GET['url']) ? $_GET['url'] : "index/index_action");
        
        //ATRIBUO O VALOR NA VARIÁVEL URL
        $this->_url = $_GET['url'];
        
    }
    
    public function getExplode() {
        
        return $this->_explode;
    }
    
    private function setExplode() {
        
        //SEPARO OS VALORES PASSADOS NA URL
        $this->_explode = explode("/", $this->_url);        
    }
    
    private function setController(){
        
        //O PRIMEIRO VALOR NO VETOR EXPLODE É O CONTROLLER
        
        $controller = str_replace('-', '_', $this->_explode[0]);
        
        $this->_controller = $controller;
        
    }
           
    private function setAction() {
        
        //VERIFICO SE A AÇÃO NÃO ESTÁ VAZIA OU NULA OU INDEX, SE FOR ATRIBUO INDEX_ACTION SENÃO ATRIBUO O VALOR PASSADO NA URL
        $ac = (!isset($this->_explode[1]) || $this->_explode[1] == NULL || $this->_explode[1] == "index" ? "index_action" : $this->_explode[1]);
        
        $ac = str_replace('-', '_', $ac);

        //ATRIBUO O VALOR NA VARIAVEL $ac
        $this->_action = $ac;
    }
    
    private function getAction() {
        return $this->_action;
    }


    private function setParams(){
    
        //DELETO DO VETOR $this->_explode O CONTROLLER E A ACTION CONTIDOS NA POSIÇAO 0 E 1
        unset($this->_explode[0], $this->_explode[1]); 
        
        //SE A ULTIMA POSIÇÃO DO VETOR EXPLODE FOR NULO, REMOVO A ULTIMA POSIÇÃO DO VETOR
        if(end($this->_explode) == NULL){
            array_pop($this->_explode);
        }
        
        //SETO O INDICE EM ZERO
        $i = 0;
        
        //SEPARO OS PARAMETROS PASSADOS NA URL ARMAZENADOS NO VETOR EXPLODE
        if(!empty($this->_explode))
        {
            foreach ($this->_explode as $val)
            {
                if($i % 2 == 0)
                {
                    $ind[] = $val;
                }
                else $valores[] = $val;
                $i++;
            }
            if(count($this->_explode) == 1)
            {
                $valores = array();
            }
        }
        else{
            
            $ind = array();
            $valores = array();
        }
        
        if((count($ind) == count($valores)) && (!empty($ind)) && (!empty($valores)))
        {
            $this->_params = array_combine($ind, $valores);
        }
        else  
        {
            $this->_params = array();
        }            
    }

    public function getParams() {
    
        return $this->_params;
    }
    
    public function getController(){
        
        return $this->_controller;
    }
    
    /**
     * 
     * @return retorna um vetor das variaveis enviadas via GET e retorna false
     */
    
    public function getParamsMethodGet(){
        
        $url = $_SERVER['REQUEST_URI'];
        
        $explode_url = explode("?", $url);
        
        unset($explode_url[0]);
        
        if (isset($explode_url[1])) {

            $key_values = str_replace("&", "=", $explode_url[1]);

            $parametros = explode("=", $key_values);

            $i = 0;

            foreach ($parametros as $value) {

                if ($i % 2 == 0) {

                    $indice[] = $value;
                } else {

                    $valores[] = $value;
                }
                $i++;
            }

            $array_combinado = array_combine($indice, $valores);

            return $array_combinado;
        }else{
            
            return false;
        }    
    }
    
    public function check_session(array $nome_sessao) {
        
        if(!isset($_SESSION))
        {
            session_start();
        }
        
        foreach($nome_sessao as $nome)
        {
            if((isset($_SESSION[$nome])) && ($_SESSION[$nome] != ""))
            {
                $check_session[$nome] = TRUE; 
            }
            else{
                $check_session[$nome] = FALSE; 
            }            
        }
        if(in_array( FALSE, $check_session) )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
        
    }
       
    public function run(){
        
        //DEFINO O NOME DO MODULO
        $name_modules = $this->_controller;
        
        //DEFINO O CAMINHO DO CONTROLLER
        $controller_path = MODULES.$name_modules."/controllers/" .$this->_controller."Controller.php";
		
       //SE ENTRAR NESSE BLOCO O CÓDIGO SEGUINTE NÃO EXECUTA
        if (!file_exists($controller_path)) {
            
//            $controller_path = CONTROLLERS."notFoundController.php";  
//            
//            require_once ($controller_path);
//            
//            $app = new notFound();
//                       
//            $app->index_action();
            
            echo "<p>Controller inexistente</p>";

            exit(); //para aqui
        }


        require_once ($controller_path);
               
        $app = new $this->_controller();
        
        if(!method_exists($app, $this->_action))
        {
//            $controller_path = CONTROLLERS."notFoundController.php";  
//            
//            require_once ($controller_path);
//            
//            $app = new notFound();
//            
//            
//            $app->index_action();

            die("Action inexistente");
            
            exit();
        }
        
        $action = $this->_action;
        
        $app->$action();
        
    }

}