<?php

class AppController extends System {

    protected function view($nome_view, $dados1 = NULL, $dados2 = NULL, $dados3 = NULL, $dados4 = NULL, $dados5 = NULL) {

        $nameModules = $this->getController();

        require_once(VIEWS . $nameModules . "/views/" . $nome_view . ".phtml");

        exit();
    }     

    protected function authenticated_view($nome_view, $dados1 = NULL, $dados2 = NULL, $dados3 = NULL, $dados4 = NULL, $dados5 = NULL) {    
        
        $appModel = new AppModel();
        
        if ($appModel->autenticar_usuario()) {

            $nameModules = $this->getController();                        

            require_once(VIEWS . $nameModules . "/views/" . $nome_view . ".phtml");

            exit();
        }
        else{
            header("location:".URL_BASE);
        }
    }
    
    protected function anti_injection($var) {        
        
        if(is_array($var))
        {
            $vetor = $var;
            
            foreach ($vetor as $key => $value) {
                
                $var[$key] = strip_tags($value);
                $var[$key] = filter_var($var[$key]);
                $var[$key] = addslashes($var[$key]);
            }
            return $var;
            
        }else{
            
            $var = strip_tags($var); //tira tags html e php
        
            $var = addslashes($var); //Adiciona barras invertidas a uma string
            
            return $var;
        }
        
    }
    
    protected function validar_json() {
        
        if(!json_last_error()){
            
            return TRUE;
        }
        else{
            return FALSE;
        }
        
    }   
    
    protected function mask($val, $mask) {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {

            if ($mask[$i] == '#') {

                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {

                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

}
