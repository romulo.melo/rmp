<?php

require_once("system/Config.php");
require_once("system/System.php");
require_once("system/AppController.php");
require_once("lib/PHPMailer/class.smtp.php");
require_once("lib/PHPMailer/class.phpmailer.php");
require_once("system/AppModel.php");

function __autoload($class) {

    $file = str_replace("Model", "", $class);

    require_once(MODULES . $file . "/" . "models/" . $file . "Model.php");
}

$system = new System;


$system->run();



